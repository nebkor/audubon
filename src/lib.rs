use argh::FromArgs;
use bevy::{
    prelude::*,
    utils::{HashMap, HashSet},
};
use bevy_spatial::{kdtree::KDTree3, SpatialAccess};

pub type NNTree = KDTree3<Toid>;

// toid stuff
const SPEED: f32 = 2.0;
const SPEED_DIFF_RANGE: f32 = 0.1; // +/- 10%
const MAX_DELTA_V: f32 = std::f32::consts::PI * 2.0; // basically 360 degrees/sec

// how far from origin before really wanting to come back
const RADIUS: f32 = 40.0;

// how close to try to stay to your buddies
const BUDDY_RADIUS: f32 = 10.0;

const MIN_ALTITUDE: f32 = 3.5;

#[derive(Debug, FromArgs, Resource)]
/// Toid Watching
pub struct Config {
    /// how many Toids to spawn
    #[argh(option, short = 't', default = "5_000")]
    pub toids: usize,
}

#[derive(Clone, Debug, Default, Deref, DerefMut, Resource)]
pub struct Positions(pub HashMap<Entity, Vec3>);

#[derive(Component, Debug, Deref, DerefMut, Clone, Default)]
pub struct Buddies(HashSet<Entity>);

#[derive(Component, Debug, Clone, Deref, DerefMut, Default)]
pub struct Velocity(Vec3);

#[derive(Component)]
pub struct Toid {
    pub speed: f32,
    pub buddies: usize,
}

#[derive(Debug, Default, Clone, Copy, Deref, DerefMut, Resource)]
pub struct LookAt(Vec3);

pub fn turkey_time(
    commands: &mut Commands,
    scene: &Handle<Scene>,
    r: &mut impl rand::prelude::Rng,
) -> Entity {
    let speed_diff = r.gen_range(-SPEED_DIFF_RANGE..=SPEED_DIFF_RANGE);
    let speed = SPEED + (SPEED * speed_diff);
    let vel = unit_vec(r) * speed;
    let buddies = r.gen_range(6..=8);
    let x = r.gen_range(-10.0..=10.0);
    let z = r.gen_range(-10.0..=10.0);
    let y = r.gen_range(0.1..=5.5);
    let spatial_bundle = SpatialBundle {
        transform: Transform::from_xyz(x, MIN_ALTITUDE + y, z),
        ..Default::default()
    };
    commands
        .spawn(spatial_bundle)
        .insert((Velocity(vel), Buddies::default(), Toid { speed, buddies }))
        .with_children(|t| {
            t.spawn(SceneBundle {
                scene: scene.to_owned(),
                ..Default::default()
            })
            .insert(Transform::default());
            //  .insert(Transform::from_rotation(Quat::from_axis_angle(
            //     Vec3::Y,
            //     -std::f32::consts::FRAC_PI_2,
            // )));
        })
        .id()
}

pub fn update_vel(
    mut toids: Query<(&Transform, &mut Velocity, &Toid, &Buddies, Entity)>,
    time: Res<Time>,
    positions: Res<Positions>,
    index: Res<NNTree>,
) {
    let dt = time.delta_seconds();
    let max_delta = MAX_DELTA_V * dt;
    for (xform, mut vel, toid, buddies, entity) in toids.iter_mut() {
        let speed = toid.speed;
        let mut dir = vel.normalize();
        let pos = xform.translation;
        let original_dir = dir;

        // find buddies and orient; point more towards further-away buddies
        for buddy in buddies.0.iter() {
            let bp = positions.get(buddy).unwrap();
            let bdir = *bp - pos;
            let dist = bdir.length();
            let rot = Quat::from_rotation_arc(dir, bdir.normalize());
            let s = (dist / (BUDDY_RADIUS * 1.2)).min(1.0);
            let rot = Quat::IDENTITY.slerp(rot, s);
            dir = rot.mul_vec3(dir).normalize();
        }

        // avoid flying into neighbors
        let min_dist = speed;
        for neighbor in index
            .within_distance(pos, min_dist)
            .iter()
            .filter(|n| n.1.is_some() && n.1.unwrap() != entity)
        {
            let bp = neighbor.0;
            let bdir = pos - bp;
            let dist = bdir.length();
            let s = 1.0 - (dist / min_dist).min(1.0);
            let rot = Quat::from_rotation_arc(dir, bdir.normalize());
            let rot = Quat::IDENTITY.slerp(rot, s);
            dir = rot.mul_vec3(dir).normalize();
        }

        // nudge toward origin if too far
        {
            let dist = pos.length();
            let toward_origin = -pos.normalize();
            let s = (dist / RADIUS).min(1.0);
            let rot = Quat::from_rotation_arc(dir, toward_origin);
            let rot = Quat::IDENTITY.slerp(rot, s);
            dir = rot.mul_vec3(dir).normalize();
        }

        // nudge up if too low
        if pos.y < MIN_ALTITUDE {
            let dh = MIN_ALTITUDE - pos.y;
            let s = (dh / MIN_ALTITUDE).min(1.0);
            let rot = Quat::from_rotation_arc(dir, Vec3::Y);
            let rot = Quat::IDENTITY.slerp(rot, s);
            dir = rot.mul_vec3(dir).normalize();
        }

        // make sure velocity doesn't change too suddenly
        let delta = dir.dot(original_dir).acos();
        if delta > max_delta {
            let s = max_delta / delta;
            let rot = Quat::from_rotation_arc(original_dir, dir);
            let rot = Quat::IDENTITY.slerp(rot, s);
            dir = rot.mul_vec3(original_dir).normalize();
        }

        **vel = dir * speed;
    }
}

pub fn update_pos(
    mut toids: Query<(&mut Transform, &Velocity, Entity), With<Toid>>,
    mut positions: ResMut<Positions>,
    mut lookat: ResMut<LookAt>,
    time: Res<Time>,
) {
    let mut new_look = Vec3::ZERO;
    let dt = time.delta_seconds();
    for (mut xform, vel, entity) in toids.iter_mut() {
        let look_at = xform.translation + vel.0;
        xform.translation += vel.0 * dt;
        xform.look_at(look_at, Vec3::Y);
        *positions.entry(entity).or_insert(Vec3::ZERO) = xform.translation;
        new_look += xform.translation;
    }
    **lookat = new_look / positions.len() as f32;
}

pub fn update_buddies(
    mut toids: Query<(&Transform, Entity, &Toid, &mut Buddies)>,
    index: Res<NNTree>,
    positions: Res<Positions>,
) {
    let d2 = (BUDDY_RADIUS * 1.5).powi(2);
    for (xform, entity, toid, mut buddies) in toids.iter_mut() {
        let pos = xform.translation;
        for buddy in buddies.clone().iter() {
            let bp = positions.get(buddy).unwrap();
            let bd2 = (*bp - pos).length_squared();
            if bd2 > d2 {
                buddies.remove(buddy);
            }
        }

        if buddies.len() < toid.buddies {
            let diff = toid.buddies - buddies.len();
            for (_, neighbor) in index
                .k_nearest_neighbour(pos, diff + 1)
                .into_iter()
                .filter(|n| n.1.is_some() && n.1.unwrap() != entity)
            {
                buddies.insert(neighbor.unwrap());
            }
        }
    }
}

pub fn update_gizmos(toids: Query<&Transform, With<Toid>>, mut gizmos: Gizmos) {
    for toid in toids.iter() {
        let pos = toid.translation;
        let up = toid.up();
        let forward = toid.forward();
        let right = toid.right();
        gizmos.ray(pos, pos + forward, Color::RED);
        gizmos.ray(pos, pos + up, Color::BLUE);
        gizmos.ray(pos, pos + right, Color::GREEN);
    }
}

pub fn rotate_camera(
    mut query: Query<&mut Transform, With<Camera>>,
    keys: Res<Input<KeyCode>>,
    lookat: Res<LookAt>,
) {
    let mut xform = query.single_mut();

    let forward = xform.forward() * 0.7;
    let right = xform.right();

    if keys.pressed(KeyCode::Right) {
        xform.translation += right;
    }
    if keys.pressed(KeyCode::Left) {
        xform.translation -= right;
    }

    if keys.pressed(KeyCode::Up) {
        if keys.pressed(KeyCode::ShiftLeft) || keys.pressed(KeyCode::ShiftRight) {
            xform.translation += Vec3::Y;
        } else {
            xform.translation += forward;
        }
    }
    if keys.pressed(KeyCode::Down) {
        if keys.pressed(KeyCode::ShiftLeft) || keys.pressed(KeyCode::ShiftRight) {
            xform.translation -= Vec3::Y;
        } else {
            xform.translation -= forward;
        }
    }

    xform.look_at(**lookat, Vec3::Y);
}

//-************************************************************************
// util
//-************************************************************************

pub fn unit_vec(r: &mut impl rand::Rng) -> Vec3 {
    let mut x1: f32 = 0.0;
    let mut x2: f32 = 0.0;
    let mut ssum = std::f32::MAX;
    while ssum >= 1.0 {
        x1 = r.gen_range(-1.0..=1.0);
        x2 = r.gen_range(-1.0..=1.0);
        ssum = x1.powi(2) + x2.powi(2);
    }
    let sqrt = (1.0 - ssum).sqrt();
    let x = 2.0 * x1 * sqrt;
    let y = 2.0 * x2 * sqrt;
    let z = 1.0 - 2.0 * ssum;
    Vec3::new(x, y, z).normalize()
}
