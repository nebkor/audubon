use std::time::Duration;

use audubon::*;
use bevy::{
    diagnostic::{FrameTimeDiagnosticsPlugin, LogDiagnosticsPlugin},
    prelude::*,
};
use bevy_spatial::{AutomaticUpdate, TransformMode};

fn main() {
    let config: audubon::Config = argh::from_env();
    App::new()
        .add_plugins(DefaultPlugins)
        .add_plugins((
            AutomaticUpdate::<Toid>::new()
                .with_transform(TransformMode::GlobalTransform)
                .with_frequency(Duration::from_millis(150)),
            FrameTimeDiagnosticsPlugin,
            LogDiagnosticsPlugin::default(),
        ))
        .insert_resource(config)
        .insert_resource(Positions::default())
        .insert_resource(ClearColor(Color::rgb(0.64, 0.745, 0.937))) // a nice light blue
        .insert_resource(AmbientLight {
            color: Color::WHITE,
            brightness: 1.0,
        })
        .insert_resource(LookAt::default())
        .add_systems(Startup, setup)
        .add_systems(Update, (update_pos, update_buddies, update_vel).chain())
        .add_systems(Update, (rotate_camera, bevy::window::close_on_esc))
        //.add_systems(Update, update_gizmos)
        .run();
}

fn setup(
    mut commands: Commands,
    mut meshes: ResMut<Assets<Mesh>>,
    mut materials: ResMut<Assets<StandardMaterial>>,
    config: Res<Config>,
    models: Res<AssetServer>,
) {
    let rand = &mut rand::thread_rng();

    commands.spawn(Camera3dBundle {
        transform: Transform::from_xyz(0., 5.0, 25.).looking_at(Vec3::new(0.0, 5.0, 0.0), Vec3::Y),
        ..default()
    });
    // plane
    commands.spawn(PbrBundle {
        mesh: meshes.add(Mesh::from(shape::Plane::from_size(500.0))),
        material: materials.add(Color::rgb(0.3, 0.5, 0.3).into()),
        ..default()
    });

    let toid_model = models.load("models/boid.glb#Scene0");

    for _ in 0..config.toids {
        let _ = turkey_time(&mut commands, &toid_model, rand);
    }

    // instructions
    commands.spawn(
        TextBundle::from_section(
            "Up and down for camera forward and back; hold shift to change height.\nLeft or right to move left or right.\nPress 'ESC' to quit.",
            TextStyle {
                font_size: 20.,
                ..default()
            },
        )
        .with_style(Style {
            position_type: PositionType::Absolute,
            top: Val::Px(12.0),
            left: Val::Px(12.0),
            ..default()
        }),
    );
}
